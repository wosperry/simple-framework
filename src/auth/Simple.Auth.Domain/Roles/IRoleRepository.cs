using Simple.Domain.Base;

namespace Simple.Auth.Domain.Roles;

public interface IRoleRepository : IRepository<Role, Guid>
{
    
}